from django.urls import path
from . import views

urlpatterns = [
    path("scatter/", views.plot, name="plot")
]
from django.shortcuts import render
from core.models import PersonSalary
import plotly.express as px
from django.db.models import Case, When, Value, CharField, Count, Q

# Create your views here.
def plot(request):
    person_salaries = PersonSalary.objects.all()
    ages = person_salaries.values_list("age", flat=True)
    salaries = person_salaries.values_list("salary", flat=True)
    color = person_salaries.values_list("education", flat=True)
    # context = {"salaries": person_salaries}

    fig = px.scatter(
        x=ages, 
        y=salaries, 
        title="Salary By Age", 
        height=800, 
        trendline="ols", 
        color=color
        )
    html = fig.to_html()

    box_fig = px.box(
        x=ages, 
        y=salaries, 
        title="Salary By Age", 
        height=800, 
    )

    box_html = box_fig.to_html()

    # aggregated data
    YEARS_PER_AGG = 5
    age_bins = [(i) for i in range(15, 85, YEARS_PER_AGG)]
    conditionals = [When(age__lte=bin, then=Value(f"{bin}")) for bin in age_bins]
    case = Case(*conditionals, output_field=CharField())


    age_groupings = person_salaries.values("education").annotate(
        less_than_20=Count("pk", filter=Q(age__lte=20)),
        less_than_25=Count("pk", filter=Q(age__lte=25)),
    )
    b = PersonSalary.objects.filter(Q(education='1. < HS Grad') &  Q(age__lte=25)) 
    print(len(b))
    print(age_groupings)



    context = {"chart": html, "box_chart": box_html}

    return render(request, "scatter.html", context)